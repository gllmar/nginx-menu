#!/bin/bash

# install dependencies on debian

sudo apt install -y nginx php-fpm

# Obtain the script's directory
script_dir=$(dirname "$(realpath "$0")")

# Define the web content directory using the script's directory
web_root="$script_dir/www/html"

# Check if the web content directory exists, if not, create it
if [[ ! -d $web_root ]]; then
    echo "Web content directory does not exist. Creating it..."
    mkdir -p $web_root
else
    echo "Web content directory already exists."
fi

# Change the ownership of the directory to pi and set the group to www-data
sudo chown -R pi:www-data $web_root

# Set the permissions so that the owner (pi) and the group (www-data) can read, write, and execute
sudo chmod -R 775 $web_root

# Set execute permissions on all parent directories of the web_root
# This loop will iterate through the directory path, setting the execute permission at each level
current_dir=$web_root
while [[ $current_dir != "/" ]]; do
    sudo chmod o+x "$current_dir"
    current_dir=$(dirname "$current_dir")
done

# Define the path to the NGINX configuration directories
sites_available="/etc/nginx/sites-available"
sites_enabled="/etc/nginx/sites-enabled"

# Define the configuration file name
config_file="default"

# Define the PHP version
php_version="8.2"
# Create the NGINX configuration block
echo "Creating NGINX configuration block for PHP website..."
cat <<EOL | sudo tee $sites_available/$config_file
server {
    listen 80;
    root $web_root;
    index index.php index.html;

    # Location block for regular web content
    location / {
        try_files \$uri \$uri/ =404;
    }

    # Location block to process PHP files
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php$php_version-fpm.sock;
    }
    include /etc/nginx/conf.d/*;
}
EOL

# Create a symbolic link to enable the configuration
echo "Enabling the NGINX configuration..."
sudo ln -s $sites_available/$config_file $sites_enabled/$config_file

# Test the NGINX configuration for syntax errors
echo "Testing the NGINX configuration..."
if sudo nginx -t; then
    # Reload NGINX to apply the new configuration
    echo "Reloading NGINX..."
    sudo systemctl reload nginx
    echo "NGINX configuration for PHP website has been applied successfully."
else
    echo "Failed to configure NGINX. Please check the configuration."
fi
