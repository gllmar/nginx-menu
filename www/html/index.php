<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NGINX menu</title>
    <style>
        /* Base styling */
        body {
            font-family: 'Arial', sans-serif;
            margin: 0;
            transition: background-color 0.3s;
        }

        #pageContent {
            background-color: #f4f4f4;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        /* Styling for the buttons */
        button {
            background-color: #007BFF;
            border: none;
            color: white;
            padding: 12px 24px;
            margin: 10px;
            font-size: 16px;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        button:hover {
            background-color: #0056b3;
        }

        /* Mobile responsiveness */
        @media (max-width: 768px) {
            button {
                font-size: 14px;
                padding: 10px 20px;
            }
        }

        /* Dark mode styles applied when checkbox is checked */
        #darkMode:checked ~ #pageContent {
            background-color: #333;
            color: #fff;
        }

        /* Style for the SVG icon when dark mode is activated */
        #darkMode:checked + label path {
            fill: #fff;
        }

        .dark-mode-toggle {
            position: fixed;
            top: 20px;
            right: 20px;
            cursor: pointer;
            z-index: 10; /* Ensure the toggle is above other elements */
        }
        .title {
    position: absolute;
    top: 2%;
    left: 50%;
    transform: translateX(-50%);  /* Centers the title horizontally */
    font-size: 2vw;  /* Responsive font size based on viewport width */
    text-align: center;
    width: 100%;
    z-index: 1;  /* Lower z-index so it's behind the buttons */
}

h1, h2 {
    margin: 0.5vw;  /* Responsive margin based on viewport width */
}

/* Reduce the title font size for very small screens */
@media (max-width: 500px) {
    .title {
        font-size: 4vw;
    }
}

/* Styling for the buttons to ensure they are always on top */
button {
    z-index: 2;  /* Higher z-index to ensure buttons are always on top */
    /* Rest of the button styles remain the same */
    width: 80%;    
}

.button-container {
    display: flex;
    flex-direction: column; /* Stack the buttons vertically */
    align-items: stretch;   /* Make all buttons stretch to the same width */
}


    </style>
</head>

<body>

    <!-- Dark Mode Toggle -->
    <input type="checkbox" id="darkMode" hidden>
    <label for="darkMode" class="dark-mode-toggle">
        <svg width="24px" height="24px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12 16C14.2091 16 16 14.2091 16 12C16 9.79086 14.2091 8 12 8V16Z" fill="#000000" />
            <path fill-rule="evenodd" clip-rule="evenodd"
                d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM12 4V8C9.79086 8 8 9.79086 8 12C8 14.2091 9.79086 16 12 16V20C16.4183 20 20 16.4183 20 12C20 7.58172 16.4183 4 12 4Z"
                fill="#000000" />
        </svg>
    </label>

    <!-- Page Content -->
    <div id="pageContent">
        
        <div class="title">
        <?php
        function getLocalIP() {
            $output = shell_exec("ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'");
            return trim($output);
        }
        $ipv4 = getLocalIP();
        $hostname = gethostname();
    ?>
    <h1><?php echo "$hostname.local"; ?></h1>
    <h2><?php echo "IPv4 : $ipv4"; ?></h2>
        </div class="button-container">

        <button onclick="window.location.href='./novnc/'">[No]VNC</button>
        <button onclick="window.location.href='./sh/'">Shell</button>
        <button onclick="window.location.href='./code/'">Code</button>
        <button onclick="window.location.href='./files/'">Files</button>
        <button onclick="window.location.href='./systemd/'">Systemd Services</button>
        <button onclick="window.location.href='./hyperhdr/'">HyperHDR</button>
        <button onclick="window.location.href='./qlcplus/'">QLCplus</button>
        <button onclick="window.location.href='./osc/'">OpenStageControl</button>
        <button onclick="window.location.href='./vlc-folder/'">VLC-loop-folder</button>
    </div>

    <script>
        // Check for saved dark mode preference and apply it
        const darkModeCheckbox = document.getElementById('darkMode');
        const savedTheme = localStorage.getItem('dark-mode-storage') || (window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light");

        if (savedTheme === "dark") darkModeCheckbox.checked = true;

        // Listen for changes and save preference
        darkModeCheckbox.addEventListener('change', function () {
            localStorage.setItem('dark-mode-storage', this.checked ? "dark" : "light");
        });
    </script>
</body>

</html>
